package crabster.rudakov.tanks

object TankNames {

    val tankNames: List<String> = listOf("M1A2 SEP v.3",
                                         "АМХ-56 Leclerc",
                                         "Т-14 «Армата»",
                                         "K2 Black Panther",
                                         "C1 Ariete",
                                         "Challenger 2",
                                         "Leopard 2А7+",
                                         "Merkava Mk4M",
                                         "Type 10",
                                         "ZTZ-99A2")

}