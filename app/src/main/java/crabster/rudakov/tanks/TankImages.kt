package crabster.rudakov.tanks

object TankImages {

    val tankImages: List<Int> = listOf(R.drawable.abrams_m1a2_sep_v3,
        R.drawable.amx_56_leclerc,
        R.drawable.armata_t14,
        R.drawable.black_panther_k2,
        R.drawable.c1_ariete,
        R.drawable.challenger_2,
        R.drawable.leopard_2a7,
        R.drawable.merkava_mk4,
        R.drawable.type_10,
        R.drawable.ztz_99a2)

}